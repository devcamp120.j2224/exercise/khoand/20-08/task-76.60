package com.devcamp.task76.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task76.entity.Employee;
import com.devcamp.task76.repository.IEmployeeRepository;

@RestController
@RequestMapping("employee")
public class EmployeesController {

    @Autowired
    IEmployeeRepository iEmployee;

    @GetMapping("/all")
    public ResponseEntity<List<Employee>> getEmployees() {

        try {
            List<Employee> listEmployee = new ArrayList<Employee>();
            iEmployee.findAll().forEach(listEmployee::add);

            if (listEmployee.size() == 0) {
                return new ResponseEntity<List<Employee>>(listEmployee, HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<List<Employee>>(listEmployee, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/detail")
    public ResponseEntity<Object> getEmployeeById(@PathVariable(name = "id", required = true) int id) {
        Optional<Employee> employeeFounded = iEmployee.findById(id);
        if (employeeFounded.isPresent())
            return new ResponseEntity<Object>(employeeFounded, HttpStatus.OK);
        else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Object> createEmployee(@RequestBody Employee employee) {
        Optional<Employee> employeeData = iEmployee.findById(employee.getId());
        try {
            if (employeeData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Drink already exsit  ");
            }
            employee.setFirstName(employee.getFirstName());
            employee.setLastName(employee.getLastName());
            employee.setExtension(employee.getExtension());
            employee.setEmail(employee.getEmail());
            employee.setOfficeCode(employee.getOfficeCode());
            employee.setReportTo(employee.getReportTo());
            employee.setJobTitle(employee.getJobTitle());

            Employee saveEmployee = iEmployee.save(employee);
            return new ResponseEntity<>(saveEmployee, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateEmployeeById(@PathVariable(name = "id") int id,
            @RequestBody Employee employee) {
        Optional<Employee> employeeData = iEmployee.findById(id);
        if (employeeData.isPresent()) {
            Employee saveEmployee = employeeData.get();

            saveEmployee.setFirstName(employee.getFirstName());
            saveEmployee.setLastName(employee.getLastName());
            saveEmployee.setExtension(employee.getExtension());
            saveEmployee.setEmail(employee.getEmail());
            saveEmployee.setOfficeCode(employee.getOfficeCode());
            saveEmployee.setReportTo(employee.getReportTo());
            saveEmployee.setJobTitle(employee.getJobTitle());

            try {
                return ResponseEntity.ok(iEmployee.save(saveEmployee));
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Employee> deleteEmployeeById(@PathVariable(name = "id") int id) {
        try {
            iEmployee.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
