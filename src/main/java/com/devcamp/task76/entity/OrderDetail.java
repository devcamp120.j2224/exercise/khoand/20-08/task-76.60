package com.devcamp.task76.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "order_details")
public class OrderDetail {

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   private int id;

   @Column(name = "price_each")
   private BigDecimal priceEach;

   @Column(name = "quantity_order")
   private int quantityOrder;

   @ManyToOne
   private Product product;

   @ManyToOne
   private Order order;

   public OrderDetail() {
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public BigDecimal getPriceEach() {
      return priceEach;
   }

   public void setPriceEach(BigDecimal priceEach) {
      this.priceEach = priceEach;
   }

   public int getQuantityOrder() {
      return quantityOrder;
   }

   public void setQuantityOrder(int quantityOrder) {
      this.quantityOrder = quantityOrder;
   }

   public Order getOrder() {
      return order;
   }

   public void setOrder(Order order) {
      this.order = order;
   }
}
