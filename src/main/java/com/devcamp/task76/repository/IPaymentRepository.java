package com.devcamp.task76.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task76.entity.Payment;

public interface IPaymentRepository extends JpaRepository<Payment, Integer> {

}
