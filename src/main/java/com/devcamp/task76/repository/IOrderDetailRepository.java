package com.devcamp.task76.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task76.entity.OrderDetail;

public interface IOrderDetailRepository extends JpaRepository<OrderDetail, Integer> {

}
